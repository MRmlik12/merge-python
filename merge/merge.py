def merge_list(a: list[str], b: list[str]) -> list[str]:
    c = list({x for x in a if x in b})
    c.sort(key=len)

    return c
