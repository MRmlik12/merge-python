# Merge python
[![coverage](https://img.shields.io/gitlab/coverage/MRmlik12/merge-python/main)](https://img.shields.io/gitlab/coverage/MRmlik12/merge-python/main)
[![pipeline](https://gitlab.com/MRmlik12/merge-python/badges/main/pipeline.svg)](https://gitlab.com/MRmlik12/merge-python/badges/main/pipeline.svg)

## Usage
```python
from merge.merge import merge_list

a = ['century', 'customer', 'democratic', 'Congress', 'customer', 'evening', 'often', 'outside', 'reveal', 'weight', 'western', 'century']
b = ['weapon', 'western', 'traditional', 'guess', 'customer', 'exist', 'democratic', 'Congress', 'evening', 'finish', 'western', 'executive']

result = merge_list(a, b)
```