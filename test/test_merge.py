from merge.merge import merge_list


def test_check_if_has_same_items_in_expected_result():
    a = [
        "century",
        "customer",
        "democratic",
        "Congress",
        "customer",
        "evening",
        "often",
        "outside",
        "reveal",
        "weight",
        "western",
        "century",
    ]
    b = [
        "weapon",
        "western",
        "traditional",
        "guess",
        "customer",
        "exist",
        "democratic",
        "Congress",
        "evening",
        "finish",
        "western",
        "executive",
    ]

    result = merge_list(a, b)
    expected_result = ["western", "evening", "Congress", "customer", "democratic"]

    assert all(item in result for item in expected_result)


def test_check_if_collection_has_not_duplicates():
    a: list[str] = [
        "century",
        "customer",
        "democratic",
        "Congress",
        "customer",
        "evening",
        "often",
        "outside",
        "reveal",
        "weight",
        "western",
        "century",
    ]
    b: list[str] = [
        "weapon",
        "western",
        "traditional",
        "guess",
        "customer",
        "exist",
        "democratic",
        "Congress",
        "evening",
        "finish",
        "western",
        "executive",
        "century",
        "century",
    ]

    result = merge_list(a, b)
    expected_result = [
        "evening",
        "western",
        "century",
        "Congress",
        "customer",
        "democratic",
    ]

    assert all(item in result for item in expected_result)
